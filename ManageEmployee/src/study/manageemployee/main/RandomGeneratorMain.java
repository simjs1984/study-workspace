package study.manageemployee.main;

import java.util.Set;

import study.manageemployee.DataWriter;
import study.manageemployee.EmployeeFileWriter;
import study.manageemployee.Writable;
import study.manageemployee.provider.DataProvider;
import study.manageemployee.provider.RandomEmployeeGenerator;

public class RandomGeneratorMain {
	public static void main(String[] args) {
		if (args.length != 2) {
			throw new IllegalArgumentException();
		}
		
		int employeeCount = Integer.parseInt(args[0]);
		DataWriter writer = new EmployeeFileWriter(args[1]);
		
		DataProvider<? extends Writable> provider = new RandomEmployeeGenerator(employeeCount);
		
		Set<? extends Writable> employees = provider.getData();
		
		writer.writeData(employees);
	}
}
