package study.manageemployee.main;

import java.util.Set;

import study.manageemployee.Writable;
import study.manageemployee.command.CalculateAvgCommand;
import study.manageemployee.command.EmployeeCommand;
import study.manageemployee.command.FindMaxCommand;
import study.manageemployee.command.FindMinCommand;
import study.manageemployee.model.Employee;
import study.manageemployee.provider.DataProvider;
import study.manageemployee.provider.EmployeeFileReader;

public class CommandMain {
	public static void main(String[] args) {
		if (args.length != 2) {
			throw new IllegalArgumentException();
		}
		
		String commandArg = args[0];
		EmployeeCommand command = null;
		
		switch (commandArg) {
		case "max":
			command = new FindMaxCommand();
			break;
		case "min":
			command = new FindMinCommand();
			break;
		case "avg":
			command = new CalculateAvgCommand();
			break;
		}
		
		DataProvider<Employee> provider = new EmployeeFileReader(args[1]);
		command.execute(provider.getData());
		
	}
}
