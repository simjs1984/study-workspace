package study.manageemployee.command;

import java.util.Set;

import study.manageemployee.model.Employee;

public class FindMaxCommand implements EmployeeCommand{

	@Override
	public void execute(Set<Employee> employees) {
		Employee maxEmployee = null;
		
		for (Employee each : employees) {
			if (maxEmployee == null) {
				maxEmployee = each;
			}
			
			if (maxEmployee.getPoint() < each.getPoint()) {
				maxEmployee = each;
			}
		}
		if (maxEmployee != null) {
			System.out.printf("Highest point employee is %s. point is %d", maxEmployee.getId(), 
					maxEmployee.getPoint());
		}
	}

}
