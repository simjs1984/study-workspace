package study.manageemployee.command;

import java.util.Set;

import study.manageemployee.model.Employee;

public interface EmployeeCommand {
	public void execute(Set<Employee> employees);
}
