package study.manageemployee.command;

import java.util.Set;

import study.manageemployee.model.Employee;

public class CalculateAvgCommand implements EmployeeCommand{

	@Override
	public void execute(Set<Employee> employees) {
		int sum = 0;
		
		for (Employee each : employees) {
			sum += each.getPoint();
		}
		
		System.out.printf("Average point is %.2f", ((float)sum/employees.size()));
	}

}
