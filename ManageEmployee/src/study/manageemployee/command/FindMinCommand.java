package study.manageemployee.command;

import java.util.Set;

import study.manageemployee.model.Employee;

public class FindMinCommand implements EmployeeCommand{

	@Override
	public void execute(Set<Employee> employees) {
		Employee minEmployee = null;
		
		for (Employee each : employees) {
			if (minEmployee == null) {
				minEmployee = each;
			}
			
			if (minEmployee.getPoint() > each.getPoint()) {
				minEmployee = each;
			}
		}
		if (minEmployee != null) {
			System.out.printf("Lowest point employee is %s. point is %d", minEmployee.getId(), 
					minEmployee.getPoint());
		}
	}


}
