package study.manageemployee.provider;

import java.util.Set;

public interface DataProvider<T> {
	public Set<T> getData();
}
