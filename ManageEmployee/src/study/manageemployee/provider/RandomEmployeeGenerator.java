package study.manageemployee.provider;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import study.manageemployee.model.Employee;

public class RandomEmployeeGenerator implements DataProvider<Employee> {
	
	public static final String TAG = RandomEmployeeGenerator.class.getSimpleName();
	private Set<Employee> employees;
	private int employeeCount;
	
	public RandomEmployeeGenerator(int count) {
		employeeCount = count;
	}

	/////////////////////// implements EmployeeReadable
	@Override
	public Set<Employee> getData() {
		if (employees == null) {
			employees = generatorEmployees();
		}
		return employees;
	}
	
	private Set<Employee> generatorEmployees() {
		Set<Employee> employees = new HashSet<>();
		Random random = new Random();
		while (employees.size() < employeeCount) {
			final int maxIdNumber = 99999; 
			int idNumber = random.nextInt(maxIdNumber);
			
			Employee employee = new Employee(String.format("LP%05d", idNumber));
			employees.add(employee);
		}
		
		for (Employee each : employees) {
			each.setPoint(random.nextInt(100));
		}
		return employees;
	}
}
