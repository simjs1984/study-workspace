package study.manageemployee.provider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import study.manageemployee.model.Employee;

public class EmployeeFileReader implements DataProvider<Employee> {
	public static final String TAG = EmployeeFileReader.class.getSimpleName();
	private Set<Employee> employees;
	
	private String fileName;

	public EmployeeFileReader(String fileName) {
		this.fileName = fileName;
	}
	
	///////////////// implements DataProvider
	@Override
	public Set<Employee> getData() {
		if (employees == null) {
			employees = readEmployees();
		}
		return employees;
	}
	
	private Set<Employee> readEmployees() {
		Set<Employee> employees = new HashSet<>();
		BufferedReader inputStream = null;
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(fileName);
			inputStream = new BufferedReader(fileReader);
			
			String employInfo;
			while ((employInfo = inputStream.readLine()) != null) {
				Employee employee = Employee.makeEmployee(employInfo);
				
				if (employee != null) {
					employees.add(employee);
				}
			}
		} catch (IOException e) {
		}
		
		return employees;
	}

}
