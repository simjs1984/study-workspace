package study.manageemployee;

import java.util.Set;

public interface DataWriter {
	public void writeData(Set<? extends Writable> data);
}
