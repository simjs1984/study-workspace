package study.manageemployee;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Set;

public class EmployeeFileWriter implements DataWriter {

	public static final String TAG = EmployeeFileWriter.class.getSimpleName();

	private String fileName;

	public EmployeeFileWriter(String fileName) {
		this.fileName = fileName;
	}

	////////////////// implements DataWriter
	@Override
	public void writeData(Set<? extends Writable> data) {
		PrintWriter pritWriter = null;
		try {
			pritWriter = new PrintWriter(new File(fileName));
			for (Writable each : data) {
				pritWriter.println(each.writableString());	
			}
		} catch (FileNotFoundException e) {
		} finally {
			if (pritWriter != null) {
				pritWriter.close();
			}
		}

	}
}
