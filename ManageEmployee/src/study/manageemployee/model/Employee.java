package study.manageemployee.model;

import study.manageemployee.Writable;

public class Employee implements Writable {
	public static final String TAG = Employee.class.getSimpleName();
	
	private String id;
	private int point;
	
	public static Employee makeEmployee(String writableString) {
		String[] split = writableString.split(" ");
		try {
			Employee employee = new Employee(split[0]);
			employee.setPoint(Integer.parseInt(split[1]));
			return employee;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public Employee(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public int getPoint() {
		return point;
	}
	
	public void setPoint(int point) {
		this.point = point;
	}
	
	////////////////////// equal
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		
		if (obj != null && obj instanceof Employee) {
			Employee another = (Employee) obj;
			
			return this.id.equals(another.id);
		}
		return false;
	}

	///////////////////// Writable
	@Override
	public String writableString() {
		return String.format("%s %d", id, point);
	}

}
